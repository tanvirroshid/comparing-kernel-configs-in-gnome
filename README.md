# Comparing Kernel Configs in GNOME

## Description
This Python script offers a practical solution for comparing two kernel configuration files (`defconfigs`) in the GNOME environment. It's designed to help developers and system administrators quickly identify differences in configurations, streamlining the process of configuring and maintaining kernel settings.

## Getting Started

### Prerequisites
- Python 3.x installed on your system.

### Installation
To get started, clone the repository to your local machine:
```bash
git clone https://gitlab.gnome.org/tanvirroshid/comparing-kernel-configs-in-gnome.git
cd comparing-kernel-configs-in-gnome
```

### Usage
To utilize the script for comparing kernel configuration files using set theory operations, execute it with the following command structure:
```bash
python3 compare_kernel_configs.py path/to/defconfig_one operation path/to/defconfig_two
```

#### Supported Set Theory Operations
- union: Combines the settings from both configurations.
- intersection: Displays settings common to both configurations.
- difference: Shows settings present in the first configuration but not in the second.
- symmetric_difference: Reveals settings that are unique to each configuration.

Replace operation with one of the supported set theory operations. The script will perform the operation and display the results.

### Example
```bash
python3 compare_kernel_configs.py path/to/defconfig_one intersection path/to/defconfig_two
```
This example command will display the common configuration settings (intersection) between the two specified defconfig files.

### Features
- Easy comparison of two kernel configuration files.
- Clear, concise output of differences.
- Fast and efficient analysis suitable for quick checks or integration into larger workflows.

### Contributing
We warmly welcome contributions to this project! If you have suggestions or improvements, please fork the repository and submit a merge request. For major changes, please open an issue first to discuss what you would like to change.

### Support
For support, questions, or to report issues, please open an issue on the GitLab repository. We aim to address queries and issues promptly.

### Authors and Acknowledgment
Special thanks to all contributors who have invested their time and effort into refining and enhancing this tool.

### License
This project is distributed under the [MIT License](LICENSE). This license permits unrestricted use, distribution, and modification in any medium, provided the original work is properly cited.

### Project Status
Currently in active development. We're continually working to improve and extend the script's capabilities. Stay tuned for updates and feel free to contribute!
