import argparse
import logging


def read_config_file(path):
    """
    Read and parse a configuration file.
    Each line of the file is treated as an element of the set.
    :param path: Path to the configuration file.
    :return: A set containing the lines of the file.
    """
    try:
        with open(path, 'r') as file:
            return {line.strip() for line in file}
    except IOError as e:
        logging.error(f"Error reading file {path}: {e}")
        raise


def compare_configs(config_one, config_two, operation):
    """
    Compare two configurations using a set theory operation.
    :param config_one: First configuration data as a set.
    :param config_two: Second configuration data as a set.
    :param operation: Set theory operation (union, intersection, etc.).
    :return: Result of the comparison as a set.
    """
    if operation == 'union':
        return config_one.union(config_two)
    elif operation == 'intersection':
        return config_one.intersection(config_two)
    elif operation == 'difference':
        return config_one.difference(config_two)
    elif operation == 'symmetric_difference':
        return config_one.symmetric_difference(config_two)
    else:
        raise ValueError(f"Unsupported operation: {operation}")


def process_line(line):
    """
    Process a line according to the specified rules:
    1) Remove lines beginning with '#'.
    2) Remove 'CONFIG_' prefix.
    3a) Replace '=y' with 'enable'.
    3b) Replace '=m' with 'module'.
    4) For lines with "=string", use `value_str config "string"`.
    5) For lines with "=number", use `value config value`.
    :param line: The line to process.
    :return: The processed line or None if the line should be skipped.
    """
    if line.startswith('#'):
        return None
    line = line.replace('CONFIG_', '')
    if '=y' in line:
        return 'enable ' + line.replace('=y', '')
    elif '=m' in line:
        return 'module ' + line.replace('=m', '')
    elif '=' in line:
        key, value = line.split('=', 1)
        if value.isnumeric():
            return f'value {key} {value}'
        else:
            return f'value_str {key} {value}'
    return line


def main():
    # Configure logging
    logging.basicConfig(level=logging.INFO)

    # Define the supported operations
    supported_operations = ['union', 'intersection', 'difference',
                            'symmetric_difference']

    # Create the argument parser
    parser = argparse.ArgumentParser(
        description='Compare two kernel configuration files using set theory\
                     operations.'
    )
    parser.add_argument('config_one_path',
                        help='Path to the first defconfig file')
    parser.add_argument('operation', choices=supported_operations,
                        help='Set theory operation for comparison')
    parser.add_argument('config_two_path',
                        help='Path to the second defconfig file')

    # Parse arguments
    args = parser.parse_args()

    try:
        # Read and process the config files
        config_one = read_config_file(args.config_one_path)
        config_two = read_config_file(args.config_two_path)

        # Perform the comparison
        result = compare_configs(config_one, config_two, args.operation)

        # Output the processed result to stdout
        for line in result:
            processed_line = process_line(line)
            if processed_line is not None:
                print(processed_line)
    except Exception as e:
        logging.error(f"An error occurred: {e}")


if __name__ == '__main__':
    main()
